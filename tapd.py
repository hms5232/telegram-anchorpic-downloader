#!/usr/bin/env python3
# coding=utf-8

"""
Repo: https://gitlab.com/hms5232/telegram-anchorpic-downloader
"""


import os

import requests
from bs4 import BeautifulSoup


def main():
	# ===== 使用者可以自訂此處參數 =====
	folder_name = "photo"  # 資料夾名稱
	filename_digits = 3  # 檔案名稱要有幾個位數
	filename_number = 1  # default start at 001
	add_header = {'User-Agent': 'Mozilla/5.0'}
	# ===== 使用者自訂參數區截止線 =====

	# 產生 request
	r = requests.get(input('Please input URL: '), headers = add_header)
	print("進行請求中...", end='\r')
	if r.status_code != 200:
		print('Request failed!')
		return
	print("進行請求中... Done")

	# 解析 response
	print("解析回應中...", end='\r')
	soup = BeautifulSoup(r.content, 'html.parser')
	img_tags = soup.find_all('img')
	print("解析回應中... Done")

	folder_path = './{}/'.format(folder_name)
	if os.path.exists(folder_path):
		action = input("同名資料夾已經存在！是否要繼續？(y/N)")
		if action.lower() != 'y':
			return
	else:
		print('建立 photo 資料夾')
		os.makedirs(folder_path)

	for index, img_tag in enumerate(img_tags):
		print('正在處理第 {} 張，共有 {} 張圖'.format(index+1, len(img_tags)), end='\r')

		img_link = 'https://telegra.ph' + img_tag.get('src')
		photo = requests.get(img_link)
		img_name = folder_path + str(filename_number).zfill(filename_digits) + os.path.splitext(img_tag.get('src'))[-1]
		with open(img_name, 'wb') as image:
			image.write(photo.content)
			image.flush()  # flushes the internal buffer

		filename_number += 1
	print()
	print("Done!")


if __name__ == '__main__':
	main()
