# telegram AnchorPic downloader
請低調使用。 `t.me/` ..._〆(°▽°*)

## 需求
1. Python 3.6 以上
2. 必須要有以下套件（可直接用 pip 安裝或使用 pipenv）
	1. request
	2. Beautiful Soup
3.  (◜◔。◔◝)足夠大的空間及權限來裝圖（´◔​∀◔`)

## 使用方式
執行 `python3 tapd.py` 後輸入目標網址，就會建立指定的資料夾（預設資料夾名稱為「photo」可自行替換程式碼中的參數，如果已存在會詢問是否繼續）之後就開始下載並編號。

## 部屬
* 一般方法
	```
	pip3 install requests
	pip3 install beautifulsoup4
	```
* 使用 pipenv：`pipenv install`

## 參考資料
* [[Python] Day14 - Python 從網路抓圖片 @ 一個小小工程師的心情抒發天地 :: 痞客邦 ::](http://dangerlover9403.pixnet.net/blog/post/207823890-%5Bpython%5D-day14---python-%E5%BE%9E%E7%B6%B2%E8%B7%AF%E6%8A%93%E5%9C%96%E7%89%87)
* [Python 如何將檔案的副檔名拔出來 - Python_NoteBook](http://wiki.alarmchang.com/index.php?title=Python_%E5%A6%82%E4%BD%95%E5%B0%87%E6%AA%94%E6%A1%88%E7%9A%84%E5%89%AF%E6%AA%94%E5%90%8D%E6%8B%94%E5%87%BA%E4%BE%86)

## 聲明
本人撰寫程式僅用於學術研究及學習，請勿用於非法用途。使用者一切行為所致皆由使用者自行承擔，與作者無任何關聯，亦不負任何責任。

## LICENSE
See [LICENSE file](https://gitlab.com/hms5232/telegram-anchorpic-downloader/-/blob/master/LICENSE).
